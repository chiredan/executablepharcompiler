<?php
/**
 * Created by IntelliJ IDEA.
 * User: Pawel
 * Date: 05.06.2016
 * Time: 12:56
 */

use Chiredan\Command\CompilePharCommand;

require __DIR__.'/vendor/autoload.php';

$application = new \Symfony\Component\Console\Application();
$application->add(new CompilePharCommand());
$application->run();